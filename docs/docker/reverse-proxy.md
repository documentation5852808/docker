# Reverse proxy

## Introduction

Quand on déploie plusieurs services sur un serveur, on peut vouloir les rendre accessibles depuis l'extérieur. On va
exposer des ports à l'extérieur des containers pour y accéder. Le problème est que ce n'est pas très pratique, car ce
n'est pas une habitude d'utiliser des ports dans les URL.

On a plusieurs solutions pour gérer ce souci :

- FQDN (Fully Qualified Domain Name) : on peut utiliser un nom de domaine pour chaque service.
  Exemple : `who1.stagiaire.dev`, `who2.stagiaire.dev`, ...
- URL par service
  Exemple : `192.168.56.200/who1`, `192.168.56.200/who2`, ...
- Faire un mix des deux = Reverse proxy : on peut utiliser un serveur qui va rediriger les requêtes vers les bons
  services. Exemple :
    - ha-proxy
    - nginx
    - Traefik
    - apache
    - squid
    - ...

## Traefik

![Logo Traefik](/traefik.png)

La plupart du temps, à chaque modification (ajout, suppression, modification...), il faut reconfigurer le reverse proxy.
Traefik est un reverse proxy dynamique avec découverte automatique des services.

Configuration composite :

- fichier de base
- Indications données au niveau de chaque service Docker

### Exemple de configuration

Pour une configuration de base, on peut utiliser le fichier `traefik.yaml` suivant :

```yaml
---
entryPoints:
  http:
    address: ":80"

  https:
    address: ":443"


api:
  insecure: true


providers:
  docker:
    endpoint: "unix:///run/docker.sock"
```

Les `entryPoints` sont des étiquettes pour les ports, on peut donc mettre les noms qu'on veut. Dans les `providers`,
comme on peut brancher Traefik à plusieurs systèmes : Docker, Kubernetes, Rancher, etc. on peut lui donner la source du
service, ici le socket Docker.

Ensuite, on crée un fichier `compose` pour lancer Traefik :

```yaml
---
services:
  traefik:
    image: traefik:v2.10
    container_name: traefik
    hostname: traefik
    ports:
      - 80:80
      - 443:443
      - 8080:8080
    volumes:
      - ./traefik.yaml:/etc/traefik/traefik.yml
      - /run/docker.sock:/run/docker.sock
    networks:
      traefiknet:

networks:
  traefiknet:
```

On expose les ports 80 et 443 pour le trafic web, et le port 8080 pour l'interface web de Traefik.

### Créer des services

Exemple de services :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    networks:
      whonet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    networks:
      whonet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    networks:
      whonet:
    restart: always

networks:
  whonet:
```

### Traefik et communication

Pour que Traefik puisse communiquer avec les services, il faut les mettre dans un réseau commun. On peut donc ajouter
les services sur le réseau `traefik_traefiknet` :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    networks:
      whonet:
      traefik_traefiknet: // [!code ++]
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    networks:
      whonet:
      traefik_traefiknet: // [!code ++]
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    networks:
      whonet:
      traefik_traefiknet: // [!code ++]
    restart: always

networks:
  whonet:
  traefik_traefiknet: // [!code ++]
    external: true // [!code ++]
```

### Choix des services à exposer

Avec cette configuration, Traefik va exposer tous les services. Parfois, on veut choisir les services à exposer. On va
donc faire évoluer le fichier `trafik.yaml` :

```yaml
---
entryPoints:
  http:
    address: ":80"

  https:
    address: ":443"


api:
  insecure: true


providers:
  docker:
    endpoint: "unix:///run/docker.sock"
    exposedByDefault: false // [!code ++]
```

... et on va ajouter des étiquettes aux services à exposer :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels: // [!code ++]
      - "traefik.enable=true" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

... ici seul `whoami-1` sera exposé.

### Traefik et réseau

Comme le service `whoami` est branché sur deux réseaux, il peut être sur l'IP de l'un ou de l'autre. Pour forcer les
services à avoir l'IP du réseau Traefik, on peut ajouter une étiquette :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

... ici `whoami-1` aura l'IP du réseau `traefik_traefiknet`.

### Traefik et URL

On peut demander à Traefik d'exposer les services avec une `host` spécifique :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

::: info
`whoami-1` après `router` dans le label est une étiquette pour le service, elle peut être n'importe quoi.
:::

### Suppression des ports

Si on expose tous les services et qu'on les expose avec une URL spécifique, on peut supprimer les ports :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports: // [!code --]
      - 81:80 // [!code --]
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports: // [!code --]
      - 82:80 // [!code --]
    labels: // [!code ++]
      - "traefik.enable=true" // [!code ++]
      - "traefik.docker.network=traefik_traefiknet" // [!code ++]
      - "traefik.http.routers.whoami-2.rule=Host(`who2.stagiaire.dev`)" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports: // [!code --]
      - 83:80 // [!code --]
    labels: // [!code ++]
      - "traefik.enable=true" // [!code ++]
      - "traefik.docker.network=traefik_traefiknet" // [!code ++]
      - "traefik.http.routers.whoami-3.rule=Host(`who3.stagiaire.dev`)" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

### Traefik et path prefix

Le premier type de routage était en `FQDN`, on peut aussi utiliser le `path prefix` :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-2.rule=Host(`who2.stagiaire.dev`)" // [!code --]
      - "traefik.http.routers.whoami-2.rule=PathPrefix(`/who2`)" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-3.rule=Host(`who3.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

... on peut alors accéder à `whoami-2` avec `http://mon-server.dev/who2`.

### Path prefix et routing

Le problème avec le `path prefix` est que quand l'URL est renvoyée vers le container, elle contiendra le "prefix",
ici `/who2`. À cause de ce dernier, l'URL ne sera pas correcte, il faut donc dynamiquement enlever le "prefix" pour que
l'URL soit correcte.

Pour cela, on peut utiliser le middleware `stripprefix`. On va alors déclarer le middleware et l'associer au service :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-2.rule=PathPrefix(`/who2`)"
      - "traefik.http.middlewares.whoami-2-stripprefix.stripprefix.prefixes=/who2" // [!code ++]
      - "traefik.http.routers.whoami-2.middlewares=whoami-2-stripprefix" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-3.rule=Host(`who3.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

::: info
`whoami-2-stripprefix` après `middlewares` dans le label est une étiquette pour le middleware, elle peut être n'importe.
Elle sert à lier le middleware au service sur la ligne suivante.
:::

### FQDN et host

On peut aussi utiliser le `FQDN` et le `host` :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-2.rule=PathPrefix(`/who2`)"
      - "traefik.http.middlewares.whoami-2-stripprefix.stripprefix.prefixes=/who2"
      - "traefik.http.routers.whoami-2.middlewares=whoami-2-stripprefix"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-3.rule=Host(`who3.stagiaire.dev`)" // [!code --]
      - "traefik.http.routers.whoami-3.rule=(Host(`who.stagiaire.dev`) && PathPrefix(`/who3`))" // [!code ++]
      - "traefik.http.middlewares.whoami-3-stripprefix.stripprefix.prefixes=/who3" // [!code ++]
      - "traefik.http.routers.whoami-3.middlewares=whoami-3-stripprefix" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

### Mise en place de l'authentification pour un service

On crée le fichier `user.password` avec le nom d'utilisateur `bob` et le mot de passe `123456789` avec `htpasswd` :

```bash
docker run --rm httpd htpasswd -nb bob 123456789 > users.password
```

Puis, on peut ajouter le middleware à Traefik :

```yaml
---
services:
  traefik:
    image: traefik:v2.10
    container_name: traefik
    hostname: traefik
    ports:
      - 80:80
      - 443:443
      - 8080:8080
    volumes:
      - ./traefik.yaml:/etc/traefik/traefik.yml
      - /run/docker.sock:/run/docker.sock
      - ./users.password:/etc/traefik/users.password // [!code ++]
    networks:
      traefiknet:

networks:
  traefiknet:
```

Puis, on ajoute le middleware `basicauth` :

```yaml
---
name: whoami
services:
  whoami-1:
    image: traefik/whoami
    hostname: whoami-1
    container_name: whoami-1
    ports:
      - 81:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-1.rule=Host(`who1.stagiaire.dev`)"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-2:
    image: traefik/whoami
    hostname: whoami-2
    container_name: whoami-2
    ports:
      - 82:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-2.rule=PathPrefix(`/who2`)"
      - "traefik.http.middlewares.whoami-2-stripprefix.stripprefix.prefixes=/who2"
      - "traefik.http.middlewares.whoami-2-basicauth.basicauth.usersfile=/etc/traefik/users.password" // [!code ++]
      - "traefik.http.routers.whoami-2.middlewares=whoami-2-stripprefix" // [!code --]
      - "traefik.http.routers.whoami-2.middlewares=whoami-2-stripprefix,whoami-2-basicauth" // [!code ++]
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

  whoami-3:
    image: traefik/whoami
    hostname: whoami-3
    container_name: whoami-3
    ports:
      - 83:80
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_traefiknet"
      - "traefik.http.routers.whoami-3.rule=Host(`who3.stagiaire.dev`)"
      - "traefik.http.routers.whoami-3.rule=(Host(`who.stagiaire.dev`) && PathPrefix(`/who3`))"
      - "traefik.http.middlewares.whoami-3-stripprefix.stripprefix.prefixes=/who3"
      - "traefik.http.routers.whoami-3.middlewares=whoami-3-stripprefix"
    networks:
      whonet:
      traefik_traefiknet:
    restart: always

networks:
  whonet:
  traefik_traefiknet:
    external: true
```

::: info
On crée le middleware `basicauth` en lui indiquant le chemin vers le fichier `users.password`, puis on l'associe au
service.
:::

### Utilisation d'un projet hors port 80

Si un service utiliser un autre port que le port 80, il est nécessaire de dire à Traefik d'utiliser un autre port.
Exemple, si on utilise un nouveau service `webapp` qui écoute en `9191` :

```yaml
---
services:
  webapp:
    image: bob2606/webapp
    container_name: webapp
    networks:
      - proxynet
    restart: always
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.webapp.rule=PathPrefix(`/webapp`)"
      - "traefik.http.services.webapp.loadbalancer.server.port=9191"
      - "traefik.http.middlewares.webapp-stripprefix.stripprefix.prefixes=/webapp"
      - "traefik.http.routers.webapp.middlewares=webapp-stripprefix"

networks:
  traefik_proxynet:
    external: true
```

C'est la ligne `traefik.http.services.webapp.loadbalancer.server.port=9191` qui indique à Traefik d'utiliser le port
`9191` pour ce service.

### Gestion des erreurs de type 4**

Si on veut gérer les erreurs de type 4**, on peut ajouter un middleware `errors`. On va commencer par prévoir un service
qui va servir les pages d'erreur. On crée un dossier `errorpages` avec les pages d'erreur, puis on ajoute un service au
fichier `compose.yaml` de Traefik :

```yaml
---
services:
  traefik:
    image: traefik:v2.10
    container_name: traefik
    hostname: traefik
    ports:
      - 80:80
      - 443:443
      - 8080:8080
    volumes:
      - ./traefik.yaml:/etc/traefik/traefik.yml
      - /run/docker.sock:/run/docker.sock
    networks:
      traefiknet:

  errorhandler: // [!code ++]
    image: nginx:1.23 // [!code ++]
    container_name: errorhandler // [!code ++]
    ports: // [!code ++]
      - 8888:80 // [!code ++]
    volumes: // [!code ++]
      - ./errorpages/:/usr/share/nginx/html // [!code ++]
    restart: always // [!code ++]
    networks: // [!code ++]
      traefiknet: // [!code ++]
    labels: // [!code ++]
      - "traefik.enable=true" // [!code ++]
      - "traefik.http.services.errorhandler.loadbalancer.server.port=80" // [!code ++]

networks:
  traefiknet:
```

Puis, on ajoute le middleware `errors` au service qui va servir les pages d'erreur :

```yaml
---
services:
  webapp:
    image: bob2606/webapp
    container_name: webapp
    networks:
      - proxynet
    restart: always
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.webapp.rule=PathPrefix(`/webapp`)"
      - "traefik.http.services.webapp.loadbalancer.server.port=9191"
      - "traefik.http.middlewares.webapp-stripprefix.stripprefix.prefixes=/webapp"
      - "traefik.http.middlewares.webapp-errors.errors.status=400-599" // [!code ++]
      - "traefik.http.middlewares.webapp-errors.errors.service=errorhandler" // [!code ++]
      - "traefik.http.middlewares.webapp-errors.errors.query=/{status}.html" // [!code ++]
      - "traefik.http.routers.webapp.middlewares=webapp-stripprefix" // [!code --]
      - "traefik.http.routers.webapp.middlewares=webapp-stripprefix,webapp-errors" // [!code ++]

networks:
  traefik_proxynet:
    external: true
```

::: info
On crée le middleware `errors` en lui indiquant le service qui va servir les pages d'erreur, puis on l'associe au
service.

On lui spécifie les erreurs à gérer, ici de 400 à 599, et le chemin vers les pages d'erreur. `status` est une variable
gérée par Traefik.

Le nom du service `errorhandler` est celui du service (nom compose) qui va servir les pages d'erreur.
:::