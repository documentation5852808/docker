# Variables d'environnement

Il est parfois nécessaire de passer des variables d'environnement à un conteneur. Par exemple, si on veut utiliser une
image `mariadb`, pour pouvoir l'initialiser, il est nécessaire de passer des variables d'environnement pour définir le
mot de passe root, le nom de la base de données, etc.

## Passer des variables d'environnement

### En ligne de commande

```bash
docker run -e MYSQL_ROOT_PASSWORD=my-secret-pw -e MYSQL_DATABASE=my_database -e MYSQL_USER=my_user -e MYSQL_PASSWORD=my_password mariadb
```

### Avec un fichier

```bash
docker run --env-file .env mariadb
```

## Exemple d'utilisation

On veut créer un conteneur `mariadb` avec les variables d'environnement suivantes :

- `MYSQL_ROOT_PASSWORD=my-secret-pw`
- `MYSQL_DATABASE=my_database`
- `MYSQL_USER=my_user`
- `MYSQL_PASSWORD=my_password`

On lui ajoute un fichier pour initialiser la base de données `init.sql` :

```sql
CREATE
DATABASE IF NOT EXISTS my_database;
USE
my_database;
CREATE TABLE IF NOT EXISTS my_table
(
    id
    INT
    AUTO_INCREMENT
    PRIMARY
    KEY,
    name
    VARCHAR
(
    255
));
```

On crée un fichier `.env` pour les variables d'environnement, on n'oublie pas de persister les données de mariadb dans
un volume. On lance alors cette commande en mode détaché :

```bash
docker run --env-file .env -v mariadb:/var/lib/mysql -v $(pwd)/sql.d:/docker-entrypoint-initdb.d -d mariadb
```