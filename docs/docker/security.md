# La sécurité dans Docker

## Introduction

Deux aspects :

- Règles de bonnes pratiques
- Lignes de défense possibles

## Règles de bonnes pratiques

Il existe plusieurs bonnes pratiques pour sécuriser un environnement Docker. En voici quelques-unes :

- Faire les mises à jour. À la fois du système que des images Docker. Sur le hub, les vulnérabilités sont listées.
- Ne pas exposer le service Docker outre mesure en TCP/IP. Par défaut, les accès TCP/IP ne sont pas activés. Le service
  n'est normalement accessible que par le socket Unix `/var/run/docker.sock`. L'utilisation de ce socket n'est
  habituellement réservé qu'à root et au groupe `docker`.
    - Si on doit exposer le service Docker, on doit sécuriser les accès distants :
        - Accès non chiffrés (2375)
        - Accès chiffrés (2376)
        - Utilisation de TLS
        - Accès sécurisés et authentifiés via SSH
        - Autres ports :
            - 2377 : pour le cluster Swarm
            - 4789 : pour le réseau overlay
- Définir un utilisateur non `root` pour les conteneurs. Cela permet de limiter les dégâts en cas de compromission. Par
  défaut, une image crée un conteneur `root`. Quelques éditeurs, comme **bitnami**, fournissent des images non root.
- Limiter l'utilisation des ressources. Plusieurs limitations sont possible :
    - `--cpus` : le nombre de coeur à utiliser
    - `--memory` : la quantité de memoire à utiliser

::: warning
Si on limite la mémoire avec `--memory`, si le conteneur n'a pas la capacité de gérer le refus de mémoire, il risque de
s'arrêter.
:::

- On peut indiquer que le conteneur sera en lecture seule. Cela permet de limiter les dégâts en cas de compromission. On
  peut ajouter `--tmpfs /tmp` pour indiquer que le conteneur sera en lecture seule mais le dossier `/tmp` sera
  accessible en écriture.

## Lignes de défense

- AppArmor
- SecComp
- NoNewPrivs
- Conteneur rootless
- Conteneur userns
- Docker rootless

### AppArmor

Mécanisme de sécurisation du noyau, au même titre que SELinux/Smack/Tomoyo. Pour savoir s'il est activé :

```bash
docker info | grep AppArmor
```
