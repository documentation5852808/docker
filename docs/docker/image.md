# Images

Dans Docker tout est basé sur des images.

![Image Docker](/docker_image_schema.png)

## Récupérer une image

```bash
docker pull <image>
```

Exemple pour l'image de `nginx` version `1.24` :

```bash
docker pull nginx:1.24
```

Il va alors la télécharger :
![Image téléchargement](/nginx_image1.24.png)

::: info
Sans indication précise, il va alors la télécharger depuis la registry communautaire
du [Docker Hub](https://hub.docker.com/).
:::

::: warning
Attention, si on ne précise pas de version, Docker va télécharger la dernière version `latest`. Si on ne le fait pas et
qu'on change de version majeur, par exemple, on risque d'avoir des surprises.
:::

## Lister les images

```bash
docker image ls
# ou l'ancienne commande
docker images
```

![Liste de mes images](/image_ls.png)

## Inspecter une image

```bash
docker image inspect <image>
```

On peut filter les informations avec `jq` :

```bash
docker image inspect <image> | jq '.[0].Config.Labels'
```

... pour obtenir les labels de l'image.
![Inspect](/image_inspect_jq.png)

## Système de couches dans Docker

### Images/containers layers

Docker utilise un système de couches pour les images et les conteneurs.

::: info
Les couches sur lesquelles sont basée l'image ne seront jamais modifiées dans le conteneur. Les modifications seront
faites dans une nouvelle couche, la couche de type "Upper Layer".
:::

Par exemple, quand on supprime un fichier dans un conteneur, il ne sera pas supprimé dans l'image. Il sera juste masqué
par un autre fichier qui a des caractéristiques très spécifiques.

![Système de fichiers](/c_file_layer.png)

C'est un système de Linux qui permet de faire croire au système de fichiers que le fichier est supprimé, mais en réalité
il est juste masqué.

### Docker diff

Pour voir les différences entre la couche supérieure et l'image de base, on peut utiliser la commande `docker diff` :

```bash
docker diff <container>
```

... et on obtient la liste des fichiers modifiés, ajoutés ou supprimés.

![Différences](/docker_diff.png)

- `C` : Changed
- `A` : Added
- `D` : Deleted

### Docker commit

Si on veut sauvegarder les modifications d'un conteneur dans une nouvelle image, on peut utiliser la
commande `docker commit` :

```bash
docker commit <container> <new_image_name>
```

... et on obtient une nouvelle image avec les modifications du conteneur.

## Supprimer une image

```bash
docker image rm <image>
```

::: warning
Attention, si l'image est utilisée par un conteneur, il faut d'abord supprimer le conteneur.
:::

## Supprimer toutes les images anonymes non utilisées

```bash
docker image prune
```

Image anonyme : image sans nom, créée par un conteneur sans option `-t`.

## Construire une image

### Dockerfile

Pour voir les instructions possibles dans un `Dockerfile`, voir le [chapitre dédié](/docker/dockerfile).
