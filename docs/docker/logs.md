# Les logs

## Général

Dans Docker, les logs sont stockés dans un fichier. Par défaut :

- 1 fichier par conteneur
- pas de limite de taille

Avec compose, on peut modifier la configuration par défaut :

```yaml
services:
  nginx:
    image: nginx
    logging:
      driver: "json-file"
      options:
        max-size: "10m"
        max-file: "10"
```

## Afficher les logs d'un conteneur

Pour afficher les logs d'un conteneur, on utilise la commande `logs` :

```bash
docker logs <nom_conteneur>
```

Par exemple, pour afficher les logs du conteneur `nginx` :

```bash
docker logs nginx
```

## Afficher les logs en temps réel

Pour afficher les logs en temps réel, on utilise la commande `logs` avec l'option `-f` :

```bash
docker logs -f <nom_conteneur>
```

## Trouver où se trouvent les logs

Pour trouver où se trouvent les logs dans un conteneur :

```bash
docker inspect --format='{{.LogPath}}' <nom_conteneur>

# ou 

docker inspect <nom_conteneur> | jq '.[].LogPath'
```

## Comment le(s) fichier(s) de logs est/sont alimenté(s) ?

