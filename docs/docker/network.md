# Les réseaux

## Lister les réseaux

```bash
docker network ls
```

Par défaut, Docker crée trois réseaux :
![Default docker networks](/docker_default_networks.png)

- `bridge`: c'est le réseau par défaut pour les conteneurs
- `host`: pour les conteneurs qui ont besoin d'accéder directement à la pile réseau de la machine hôte
- `none`: pour les conteneurs qui n'ont pas besoin d'accès réseau

::: info
Remarques :

- Il n'y a pas de résolution de nom dans le réseau `bridge`
- Il n'est pas possible d'attribuer d'IP statique dans le réseau `bridge` et sur tout réseau dont on n'a pas défini le
  subnet

:::

Quand il n'y a pas de conteneurs dans les réseaux, ils sont indiqués en `DOWN` par Linux. Dès qu'on en a à l'intérieur,
ils passent en `UP`.

![Liste des conteneurs](/ip_list_networks.png)

## Réseaux et conteneurs

Pour connaître les conteneurs dans un réseau, on peut utiliser la commande :

```bash
docker network inspect <network>
```

Si on ne veut que les IDs des conteneurs, on peut utiliser `jq` pour filtrer :

```bash
docker network inspect bridge | jq '.[].Containers|keys'
```

... et ainsi obtenir la liste des conteneurs dans le réseau `bridge` et ne lister que leurs IDs.

On peut également, lister les IPs des conteneurs avec leurs noms dans un réseau :

```bash
docker network inspect bridge | jq '.[].Containers[]|.Name + " : " + .IPv4Address'
```

Résultat :
![Liste des conteneurs dans le réseau](/network_ls_name-ip.png)

### Schéma de conteneurs dans un réseau

![Schéma de conteneurs dans un réseau](/schema_container.png)
... on voit ici que les conteneurs `nginx` et `busy` sont dans le réseau `bridge` et ont respectivement des IPs sur ce
réseau.

Le problème est que si on stocke un site internet dans le conteneur `nginx`, il ne sera pas accessible depuis
l'extérieur, exemple depuis un navigateur. En effet, la machine hôte ne peut accéder au port `80` du conteneur `nginx`.

### Publier un port sur le réseau

Pour résoudre ce problème, on peut publier un port du conteneur sur le réseau. On peut le faire lors de la création du
conteneur :

```bash
docker run --publish <host_port>:<container_port> <image>
```

- `--publish` ou `-p` : pour publier un port
- `<host_port>` : le port de la machine hôte
- `<container_port>` : le port du conteneur

Si on regarde avant l'ouverture du port, on voit que la machine hôte n'a que le port `22` d'ouvert :

![Ports ouverts](/port_ls.png)

Avec le conteneur `nginx`, on va alors ouvrir le port `80` du conteneur `nginx` sur le port `80` de la machine hôte :

```bash
docker run --detach --name www --rm --publish 80:80 nginx:1.24
```

... et on voit que le port `80` est ouvert :

![Ports ouverts avec le 80](/port_ls_80.png)

Ce routing est fait grâce au service `iptables` :

![iptables](/iptables_example.png)

On peut donc faire évoluer notre schéma :
![Schéma de conteneurs dans un réseau avec routing port](/schema_container_ports.png)
... le réseau `bridge` va alors publier le port `80` du conteneur `nginx` sur le port `80` de la machine hôte.

## Créer un réseau

Pour créer un réseau, on peut utiliser la commande :

```bash
docker network create <network>
```

Exemple :

```bash
docker network create stagenet
```

2680, 0476, 0199

### Affecter une IP statique

On peut lui attribuer un subnet :

```bash
docker network create --subnet 172.18.0.0/16
```

... et on voit que le réseau est bien créé :
![List des réseaux sur l'hôte](/network_list_host.png)

::: info
On voit bien comme indiqué plus haut que le réseau `stagenet` nouvellement créé est `DOWN` car il n'y a pas de
conteneurs dedans.
:::

::: info
Le `16` dans `172.18.0.0/16` est le nombre de bits pour le subnet. Cela signifie que l'on peut avoir `2^16` adresses IP
car on peut utiliser les deux derniers bits (`0.0`) pour lister les adresses IP.
:::

### Mettre un nom

On voit sur l'impression écran ci-dessus que le nom du réseau est `br-<hash>`. On peut lui donner un nom :

```bash
docker network create -o com.docker.network.bridge.name=stagenet stagenet
```

![List des réseaux sur l'hôte avec nom](/network_list_host_named.png)

## Supprimer un réseau

Pour supprimer un réseau, on peut utiliser la commande :

```bash
docker network rm <network>
```

## Supprimer tous les réseaux non utilisés

Pour supprimer tous les réseaux non utilisés, on peut utiliser la commande :

```bash
docker network prune
```

Réseau non utilisé : réseau sans conteneurs.

## Attacher un conteneur à un réseau

Pour attacher un conteneur à un réseau, on peut utiliser la commande :

```bash
docker network connect <network> <container>
```

... et pour le détacher :

```bash
docker network disconnect <network> <container>
```

::: info
Docker ne permet pas le routage entre réseaux. Si on veut que deux conteneurs communiquent, il faut :

- connecter les deux conteneurs à un même réseau
- en exposant les ports des conteneurs sur l'hôte
- faire en sorte que les deux conteneurs partagent le même espace de noms réseau

:::