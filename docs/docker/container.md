# Conteneurs

## Lister les conteneurs

```bash
docker container ls
# ou l'ancienne commande
docker ps
```

::: warning
Attention, `docker container ls` ne liste que les conteneurs en cours d'exécution. Pour lister tous les conteneurs, il
faut ajouter l'option `-a`.
:::

## Créer et lancer un conteneur

### Créer un conteneur

```bash
docker run <image>
```

Il va alors créer un conteneur à partir de l'image spécifiée et le démarrer. Le conteneur sera créé en mode attaché,
c'est-à-dire que les logs seront affichés dans le terminal.

Pour l'arrêter, il faut faire `Ctrl+C`. Quand il est arrêté, il n'est pas supprimé et ceci car :

- on peut le redémarrer
- on ne perd pas les logs
- on ne perd pas les modifications

Si on veut le redémarrer, on peut utiliser la commande `docker start <container>`.

Pour plus de détails, voir [docker run](/commands/docker.html#docker-run).

### Créer et lancer un conteneur en mode détaché

```bash
docker run -d <image>
```

### Créer et lancer un conteneur en tant qu'un autre utilisateur

```bash
docker run --user <user> <image>
```

::: info
`<user>` peut être un nom d'utilisateur ou un UID. Pour connaître l'UID d'un utilisateur, on peut utiliser la
commande `id -u <user>`.
:::

## Arrêter un conteneur

```bash
docker stop <container>
```

## Supprimer un conteneur

```bash
docker rm <container>
```

::: info
Pour supprimer un conteneur en cours d'exécution, il faut ajouter l'option `-f` pour forcer la suppression.
:::

## Supprimer tous les conteneurs non utilisés

```bash
docker container prune
```

Conteneur non utilisé : conteneur arrêté.

## Redémarrer un conteneur

Dans un cas comme NGinx, on doit redémarrer le conteneur pour prendre en compte les modifications du fichier de
configuration. Pour cela, on peut utiliser la commande `restart`. Cela va arrêter le conteneur et le
redémarrer sans le supprimer.

```bash
docker restart <container>
```
