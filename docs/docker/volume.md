# Volumes

C'est un objet Docker qui est associé à un répertoire à sa création. Quand on fait un `inspect`, on retrouve le chemin
du répertoire associé.

```bash
stagiaire@deby:~$ docker volume inspect data
[
    {
        "CreatedAt": "2024-02-20T09:53:40+01:00",
        "Driver": "local",
        "Labels": null,
        "Mountpoint": "/var/lib/docker/volumes/data/_data",
        "Name": "data",
        "Options": null,
        "Scope": "local"
    }
]
```

Ces volumes souvent utilisés pour stocker les données produites par le conteneur (SGBD, LDAP, Redis, etc.). Pour ce qui
est des autres données (fichiers de configuration, site web, etc.), on peut utiliser les bind mounts.

## Créer un volume

```bash
docker volume create data
```

## Supprimer un volume

```bash
docker volume rm data
```

## Supprimer tous les volumes anonymes non utilisés

```bash
docker volume prune
```

Volume anonyme : volume sans nom, créé par un conteneur sans option `-v`.

## Rattacher un volume ou un "bind mount" à un conteneur

### Volume

Lorsqu'on lance un conteneur, on peut rattacher un volume à un répertoire du conteneur. Par exemple, pour rattacher le
volume `data` à `/usr/share/nginx/html/` :

```bash
docker run -v data:/usr/share/nginx/html/ nginx
```

### Bind mount

::: info
La différence entre un volume et un "bind mount" est que le volume est géré par Docker, alors que le "bind mount" est un
service du système hôte.
:::

#### Montage "classique"

On peut aussi monter un répertoire de la machine hôte dans un conteneur. Par exemple, pour monter le répertoire
`/home/stagiaire/site/` dans le répertoire `/usr/share/nginx/html/` du conteneur `nginx` :

```bash
docker run -v /home/stagiaire/site/:/usr/share/nginx/html/ nginx
```

#### Montage avec `--mount`

On peut aussi avec la commande `--mount` :

```bash
docker run --mount type=bind,source=/home/stagiaire/site/,target=/usr/share/nginx/html/ nginx
```

::: info
Avec cette méthode, si le dossier source n'existe pas, Docker renvoie une erreur, cela évite de monter un dossier par
erreur.
:::

#### Montage en lecture seule

On peut aussi changer le type de montage. Par exemple, pour monter le répertoire `/home/stagiaire/site/` en lecture
seule :

```bash
docker run -v /home/stagiaire/site/:/usr/share/nginx/html/:ro nginx
```

::: info
Les valeurs possibles sont `ro` pour read-only et `rw` pour read-write.
:::