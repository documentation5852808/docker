# Dockerfile

## Construire une image

Pour construire une image, on utilise un fichier `Dockerfile` qui contient les instructions pour construire l'image. Par
exemple, si on souhaite construire une image PHP en lui installant le module `pdo_mysql`, on peut utiliser
le `Dockerfile` suivant :

```dockerfile
FROM php:8.2-fpm

RUN docker-php-ext-install pdo_mysql
```

... puis on peut construire l'image avec la commande :

```bash
docker build -t php-mariadb:8.2-fpm .
```

Pour information : Docker va utiliser un système de cache si on lui demande de refaire une commande qu'il a déjà faite.
Premier lancement :

```bash
stagiaire@deby:~/php$ docker build . -t php-mariadb:8.2-fpm
[+] Building 6.7s (6/6) FINISHED                                                                                                                                                                                                                                           docker:default
 => [internal] load build definition from Dockerfile                                                                                                                                                                                                                                 0.0s
 => => transferring dockerfile: 92B                                                                                                                                                                                                                                                  0.0s
 => [internal] load metadata for docker.io/library/php:8.2-fpm                                                                                                                                                                                                                       0.0s
 => [internal] load .dockerignore                                                                                                                                                                                                                                                    0.0s
 => => transferring context: 2B                                                                                                                                                                                                                                                      0.0s
 => [1/2] FROM docker.io/library/php:8.2-fpm                                                                                                                                                                                                                                         0.1s
 => [2/2] RUN docker-php-ext-install pdo_mysql                                                                                                                                                                                                                                       6.3s
 => exporting to image                                                                                                                                                                                                                                                               0.1s 
 => => exporting layers                                                                                                                                                                                                                                                              0.1s 
 => => writing image sha256:80f1c503bf7ebf3d0e52fec5088b8e71e214f35e20545061ce162888819ff084                                                                                                                                                                                         0.0s 
 => => naming to docker.io/library/php-mariadb:8.2-fpm     
```

Deuxième lancement :

```bash
stagiaire@deby:~/php$ docker build . -t php-mariadb:8.2-fpm-2
[+] Building 0.1s (6/6) FINISHED                                                                                                                                                                                                                                           docker:default
 => [internal] load build definition from Dockerfile                                                                                                                                                                                                                                 0.0s
 => => transferring dockerfile: 92B                                                                                                                                                                                                                                                  0.0s
 => [internal] load metadata for docker.io/library/php:8.2-fpm                                                                                                                                                                                                                       0.0s
 => [internal] load .dockerignore                                                                                                                                                                                                                                                    0.0s
 => => transferring context: 2B                                                                                                                                                                                                                                                      0.0s
 => [1/2] FROM docker.io/library/php:8.2-fpm                                                                                                                                                                                                                                         0.0s
 => CACHED [2/2] RUN docker-php-ext-install pdo_mysql                                                                                                                                                                                                                                0.0s
 => exporting to image                                                                                                                                                                                                                                                               0.0s
 => => exporting layers                                                                                                                                                                                                                                                              0.0s
 => => writing image sha256:80f1c503bf7ebf3d0e52fec5088b8e71e214f35e20545061ce162888819ff084                                                                                                                                                                                         0.0s
 => => naming to docker.io/library/php-mariadb:8.2-fpm-2
```

... on voit que la commande `=> CACHED [2/2] RUN docker-php-ext-install pdo_mysql` vient du cache.

::: tip
Si le `Dockerfile` s'appelle autrement, on peut préciser le nom du fichier avec l'option `-f` :

```bash
docker build -f <Dockerfile> -t <image_name> .
```

:::

On peut ensuite envoyer cette nouvelle image sur [une registry](/docker/registry).

## Instruction possibles d'un Dockerfile

### `FROM`

Le `Dockerfile` doit commencer par une instruction `FROM` qui indique l'image de base.

### `WORKDIR`

L'instruction `WORKDIR` permet de définir le répertoire de travail pour les instructions suivantes :

```dockerfile
WORKDIR /path/to/workdir
```

::: info
Deux informations :

- Si le répertoire n'existe pas, Docker le crée.
- Toutes les instructions suivantes seront exécutées dans ce répertoire.

:::

::: warning
L'instruction `WORKDIR`, même s'il ne change pas l'arborescence des fichiers, crée une nouvelle couche dans l'image.
C'est dû au fait que Docker crée un nouveau conteneur pour chaque instruction `RUN`, `COPY`, `ADD`, `WORKDIR`, etc. et
enregistre le résultat dans une nouvelle image.
:::

### `ARG`

L'instruction `ARG` permet de définir des variables qui peuvent être utilisées dans le `Dockerfile`. Par exemple :

```dockerfile
ARG MY_NAME="John Doe"
```

... et on peut utiliser cette variable dans le `Dockerfile` :

```dockerfile
ENV NAME ${MY_NAME}
```

... et on peut passer une valeur à cette variable lors de la construction de l'image :

```bash
docker build --build-arg MY_NAME="Jane Doe" -t my-image .
```

Si on ne passe pas de valeur, la valeur par défaut sera utilisée. Pour définir une valeur par défaut :

```dockerfile
ARG MY_NAME="John Doe"
# ou
ENV NAME ${MY_NAME:-"John Doe"}
```

### `ENV`

L'instruction `ENV` permet de définir des variables d'environnement dans l'image. Par exemple :

```dockerfile
ENV NAME="John Doe"
```

... et on peut utiliser cette variable dans le `Dockerfile` :

```dockerfile
RUN echo "Hello, ${NAME}"
```

### `USER`

L'instruction `USER` permet de définir l'utilisateur qui exécute les instructions suivantes :

```Docker
USER user
```

::: info
On peut revenir à l'utilisateur `root` avec l'instruction `USER root`.
:::

### `COPY`

Pour copier des fichiers depuis le système de fichiers hôte vers l'image, on utilise l'instruction `COPY` :

```dockerfile
COPY ./init.sql /docker-entrypoint-initdb.d/
```

### `ADD`

L'instruction `ADD` permet également de copier des fichiers depuis le système de fichiers hôte vers l'image, mais
contrairement à [`COPY`](#copy) mais elle permet aussi de télécharger des fichiers depuis une
URL :

```dockerfile
ADD https://example.com/big.tar.xz /usr/src/things/
```

... ou encore d'extraire des fichiers depuis une archive et de les copier dans l'image :

```dockerfile
ADD big.tar.xz /usr/src/things/
```

### `LABEL`

L'instruction `LABEL` permet d'ajouter des métadonnées à une image. Par exemple :

```dockerfile
LABEL "com.example.vendor"="ACME Incorporated"
LABEL com.example.label-with-value="foo"
LABEL version="1.0"
LABEL description="This text illustrates \
that label-values can span multiple lines."
```

### `RUN`

L'instruction `RUN` permet d'exécuter des commandes dans l'image. Par exemple :

```dockerfile
RUN apt-get update && apt-get install -y \
    git \
    curl
```

... `RUN` exécute les commandes dans un nouveau conteneur provisoire et enregistre le résultat dans une nouvelle image.

::: warning
Chaque instruction `RUN` va créer un layer, il est donc recommandé de regrouper les commandes `RUN` pour éviter de créer
des couches inutiles. Par exemple :

```dockerfile
RUN apt-get update && apt-get install -y \
    git \
    curl
```

... est préférable à :

```dockerfile
RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y curl
```

:::

### `ENTRYPOINT`

L'instruction `ENTRYPOINT` permet de définir une commande qui sera exécutée lorsqu'un conteneur basé sur l'image sera
lancé. Exemple dans l'image `mariadb` l'`ENTRYPOINT` sert à initialiser la base de données et les fichiers nécessaires à
mariadb.

Exemple d'entrypoint :

```dockerfile
ENTRYPOINT ["echo", "Hello"]
```

::: info
L'`ENTRYPOINT` peut être un script shell ou un programme exécutable. Si c'est le cas, il faut penser à l'ajouter dans l'
image avec un [`COPY`](#copy) ou un [`ADD`](#add).
:::

::: info
L'`ENTRYPOINT` peut être surchargé lors du lancement du conteneur avec l'option `--entrypoint` :

```bash
docker run --entrypoint /bin/bash <image>
```

:::

#### Exemple de script shell

```bash
#!/bin/bash

date

exec "$@"
```

... ce script affiche la date, puis exécute la commande passée en argument :

```bash
docker run <image> echo "Hello"
```

... affichera :

```bash
Tue 15 Mar 2022 10:00:00 UTC
Hello
```

### `CMD`

Deux cas possibles :

- Si l'instruction `CMD` est utilisée seule, elle définit la commande par défaut qui sera exécutée lorsqu'un conteneur
  basé sur l'image sera lancé.
- Si l'instruction `CMD` est utilisée avec [`ENTRYPOINT`](#entrypoint), elle définit les arguments
  qui seront passés à la commande définie par [`ENTRYPOINT`](#entrypoint).

::: warning
S'il y a plusieurs instructions `CMD` dans le `Dockerfile`, seule la dernière sera prise en compte.
:::

### `EXPOSE`

L'instruction `EXPOSE` permet de définir les ports sur lesquels le conteneur écoute. Par exemple :

```dockerfile
EXPOSE 80
```

... permet de dire que le conteneur écoute sur le port 80. Ce sera le port interne du conteneur, il faudra le mapper
avec le port de la machine hôte lors du lancement du conteneur, par exemple :

```bash
docker run -p 8080:80 <image>
```

... pour mapper le port 8080 de la machine hôte sur le port 80 du conteneur.

## Exemple de `Dockerfile`

```dockerfile
FROM debian:12

COPY sql.d/stage.sql /usr/src
ADD site.tar.gz /opt

LABEL auteur="Bob"

ARG LOGIN=bob
RUN groupadd admin &&  useradd -m -G admin -s /bin/bash ${LOGIN}
ENV USER ${LOGIN}
USER ${LOGIN}
```

## Naviguer dans une image

Pour explorer une image et voir les modifications au niveau des couches :

```bash
docker run -it -v /run/docker.sock:/run/docker.sock wagoodman/dive <image_a_explorer>
```