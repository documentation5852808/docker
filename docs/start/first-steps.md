# Premiers pas

## L'environnement Docker

Un conteneur, c'est 4 briques de base :

- [image](/docker/image.html) : arborescence de départ et des informations
- [conteneur](/docker/container.html) : instance d'une image
- [réseau(x)](/docker/network.html) : pour connecter les conteneurs entre eux
- [volume(s)](/docker/volume.html) : pour persister les données

![environnement Docker](/docker_env.png)

## Les commandes de base

```bash
docker <object> <action> <options>
```

- `object` : image, container, network, volume
- `action` : create, run, start, stop, rm, ps, inspect, logs, exec, ...
