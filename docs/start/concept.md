# Docker

## La containerisation vs la virtualisation

La containerisation est un moyen d'avoir un environnement de développement ou de production isolé, sans avoir à
installer des dépendances sur la machine hôte.

La containerisation n'a rien à voir avec la virtualisation. En effet, les conteneurs partagent le même noyau que la
machine hôte, ce qui les rend plus légers et plus rapides à démarrer. La virtualisation, quant à elle, nécessite un
hyperviseur pour gérer les machines virtuelles.

Exemple de virtualisation :
![Exemple de virtualisation](/virtualisation.png)

Exemple de containerisation :
![Exemple de containerisation](/containerisation.png)

## La containerisation avec Docker

Elle repose sur un ensemble de fonctionnalités du noyau Linux, exemples :

- `chroot` : pour changer le répertoire racine d'un processus. Le processus est alors confiné dans un sous-arbre du
  système de fichiers et ne voit plus le reste du système qui se trouve au-dessus.
- Espace de nommage (`namespace`) : pour isoler les ressources du système
    - User : pour isoler les utilisateurs
    - Net (Network) : pour isoler les interfaces réseau
    - UTS (Hostname) : pour isoler les noms d'hôtes
    - Mnt (Mount) : pour isoler les points de montage
    - PID : pour isoler les processus
    - IPC : pour isoler les mémoires partagées
    - Cgroup : pour isoler les ressources
    - Time : pour isoler le temps
- `Montage bind` : pour monter un répertoire dans un autre
- `IpTables` : pour isoler les règles de pare-feu
- `overlayfs` : pour superposer des systèmes de fichiers
- `seccomp` : pour restreindre les appels systèmes disponibles
- `capabilities` : pour restreindre les privilèges des processus
- `unionfs` : pour superposer des systèmes de fichiers
- `cgroups` : pour limiter les ressources utilisées par les processus

Docker va créer les conteneurs via le service `containerd` qui va lui-même utiliser les fonctionnalités du noyau Linux
et utiliser le service `runc` pour démarrer les conteneurs.
`containerd` est aujourd'hui un projet de la fondation `Cloud Native Computing Foundation` (CNCF) et sert dans d'autres
projets comme `Kubernetes`.

