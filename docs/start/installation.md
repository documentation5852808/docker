# Installation

Pour installer Docker sur une machine Debian, d'abord récupérer les librairies nécessaires.

```bash
# Mise à jour des caches apt
apt update
# Installation des packages pré-requis
apt install gpg curl git sudo tree jq
```

Ensuite, on ajoute le repository Docker à la liste des sources apt.
```bash
# Récupération de la clé publique docker
curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor --batch -o /usr/share/keyrings/docker-archive-keyring.gpg
# Ajout du catalogue Docker
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
```

Enfin, on installe Docker et on ajoute notre user au groupe `docker`.
```bash
# Rafraîchissement des caches apt
apt update
# Installation Docker-ce
apt install docker-ce
# Ajout du compte stagiaire dans le groupe docker
usermod -a -G docker <username>
```