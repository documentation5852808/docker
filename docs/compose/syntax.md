# Syntaxe

Si on souhaite faire cette commande Docker en Compose :

```bash
docker run -d \
           --name busy \
           --publish 9000:9000 \
           -v data:/partage \
           -v ./config:/opt/config \
           -e USER=bob \
           -e PASSWORD=secret \
           --network stagenet \
           busybox
```

On peut le faire en Compose avec :

```yaml
---
version: "3.7"

services:
  busy:
    image: busybox:latest
    container_name: busy
    ports:
      - "9000:9000"
    volumes:
      - data:/partage
      - ./config:/opt/config
    environment:
      - USER=bob
      - PASSWORD=secret
    command:
      - sleep
      - infinity
    networks:
      - stagenet
    restart: always

volumes:
  data:
networks:
  stagenet:
```

::: tip
Pour les variables d'environnement, on peut utiliser un dictionnaire :

```yaml
environment:
  USER: bob
  PASSWORD: secret
```

:::

::: info
Pour les volumes et les réseaux, on peut utiliser une séquence :

```yaml
volumes:
  data:
networks:
  stagenet:
```

ou

```yaml
volumes:
  data:
    external: true
networks:
  stagenet:
    external: true
```

Selon si le volume ou le réseau préexiste ou non.
:::

On peut valider alors notre fichier de configuration avec :

```bash
docker compose config
```

... et il nous retournera un message de confirmation avec la syntaxe complète qu'il a interprétée :

```bash
stagiaire@deby:~/stagiaire$ docker compose config
name: stagiaire
services:
  busy:
    command:
      - sleep
      - infinity
    container_name: busy
    environment:
      PASSWORD: secret
      USER: bob
    image: busybox:latest
    networks:
      stagenet: null
    ports:
      - mode: ingress
        target: 9000
        published: "9000"
        protocol: tcp
    restart: always
    volumes:
      - type: volume
        source: data
        target: /partage
        volume: {}
      - type: bind
        source: /home/stagiaire/stagiaire/config
        target: /opt/config
        bind:
          create_host_path: true
networks:
  stagenet:
    name: stagiaire_stagenet
volumes:
  data:
    name: stagiaire_data
```

::: info
On voit plusieurs choses :

- il a ajouté un nom de projet par défaut => `stagiaire`. C'est le nom du répertoire où se trouve le fichier de
  configuration.
- il préfixe les noms des volumes et des réseaux avec le nom du projet.
- il a ajouté des options par défaut pour les volumes et les réseaux.

:::

On peut alors lancer notre service avec :

```bash
docker compose up -d
```

::: info
Le `-d` est pour "detached" ou "détaché". Cela permet de lancer le service en arrière-plan.
:::
