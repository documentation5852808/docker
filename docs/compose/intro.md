# Introduction

Quand on utilise Docker directement, les lignes de commandes qu'on doit faire pour créer, lancer, arrêter,
supprimer, ... un conteneur, une image, un volume, ... sont nombreuses et pas toujours évidentes. De plus, quand on veut
faire une "stack" complète, on doit faire plusieurs commandes. Par exemple, pour faire une "stack" LAMP, on doit lancer
au moins trois conteneurs : un pour Apache, un pour MariaDB et un pour PHP. Et pour les lier, on doit créer des réseaux.

Pour simplifier ces tâches, on peut utiliser Docker Compose. C'est un outil qui permet de décrire une "stack" dans un
seul fichier YAML. Ce fichier contient la description des services, des réseaux, des volumes, ... nécessaires pour la "
stack". Ensuite, on peut lancer, arrêter, supprimer, ... la "stack" avec une seule commande.

## YAML Syntaxe

::: info
Pendant longtemps, YAML a été considéré comme un acronyme pour "Yet Another Markup Language". Mais en 2006, la
signification a été changée. Maintenant, il est officiellement "YAML Ain't Markup Language".

C'est un format de sérialisation de données. Il est souvent utilisé pour les fichiers de configuration.
:::

::: warning
L'indentation est très importante en YAML. Elle permet de définir les niveaux de hiérarchie. Elle doit être faite avec
des espaces, pas des tabulations.
:::

[Tutoriel](https://www.w3schools.io/file/yaml-introduction/)

### Valider un fichier YAML

La syntaxe YAML peut être validé, par la librairie `yamllint` (qui doit être installé via `apt`) :

```bash
yamllint <file>
```

### Scalaire

```yaml
key: value
```

### Séquence ou liste ou tableau indicé

```yaml
key:
  - value1
  - value2
  - value3
```

... ou ...

```yaml
key: [ "value1", "value2", "value3" ]
```

### Dictionnaire ou tableau associatif

```yaml
key:
  subkey1: value1
  subkey2: value2
  subkey3: value3
```

... ou ...

```yaml
key: { subkey1: value1, subkey2: value2, subkey3: value3 }
```

## Docker Compose

Avant, il était nécessaire d'installer Docker Compose séparément. Mais maintenant, il est inclus dans Docker Desktop via
un plugin qui est présent, par défaut, à l'installation de Docker.

Ancienne syntaxe :

```bash
docker-compose <command>
```

Nouvelle syntaxe :

```bash
docker compose <command>
```

Par convention, le fichier de configuration de Docker Compose était nommé `docker-compose.yml`. Mais maintenant, il peut
être nommé `compose.yml`.

Pour valider un fichier de configuration de Docker Compose, on peut utiliser la commande :

```bash
docker compose config
```