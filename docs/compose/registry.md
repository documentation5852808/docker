# Registry

Pour stocker des images sur un serveur, on peut se créer un registre. Un registre est un serveur qui stocke des images
Docker. Il peut être public ou privé.

## Créer un registre

Pour créer un registre, on peut utiliser l'image `registry` de Docker. On peut la télécharger avec la commande suivante:

```yaml
---
services:
  registry:
    image: registry:2.8
    container_name: registry
    ports:
      - 5000:5000
    volumes:
      - registry_db:/var/lib/registry
    restart: always

volumes:
  registry_db:
```

## Préparation de l'image

Pour pouvoir stocker une image sur un registre, il faut que l'image ait un nom qui correspond à l'adresse du registre.
Par exemple, si on veut stocker l'image `lamp-php` sur le registre `localhost:5000`, il faut que l'image
s'appelle `localhost:5000/lamp-php`.

Pour renommer une image, on peut utiliser la commande `docker tag`:

```bash
docker tag lamp-php:8.3-fpm localhost:5000/lamp-php:8.3-fpm
```

## Pousser une image

Pour stocker une image sur un registre, on peut utiliser la commande `docker push`:

```bash
docker push localhost:5000/lamp-php:8.3-fpm
```

## Utiliser l'image

Dans notre `compose.yaml`, on peut utiliser l'image stockée sur le registre:

```yaml
---
services:
  # ...

  php:
    image: localhost:5000/lamp-php:8.3-fpm
    build:
      context: ./php
      dockerfile: Dockerfile
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    networks:
      - stagenet

  # ...

volumes:
  lamp_data:
```

::: info
Quand on fait un `docker info`, tout en bas, on a une ligne indiquée "Insecure Registries". En dehors des registres
listés dans ce tableau, Docker les interrogera en HTTPS.

Donc deux solutions pour y accéder :

- ajouter un registre à cette liste, on peut éditer le fichier `/etc/docker/daemon.json` et ajouter la ligne suivante:

```json
{
  "insecure-registries": [
    "192.168.56.200:5000"
  ]
}

```

Ensuite, on redémarre le service Docker:

```bash
sudo systemctl restart docker
```

- sécuriser le registre avec un certificat SSL.

:::

## Sécuriser le registre

