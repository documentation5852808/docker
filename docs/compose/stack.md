# Stack complète

## Préparation des fichiers

Architecture du projet :

```plaintext
.
├── conf-nginx
│   └── default.conf
├── compose.yml
├── init-db
│   └── init.sql
└── site
    ├── index.html
    ├── index.php
    └── testdb.php
```

### Le fichier `conf-nginx/default.conf`

```nginx:line-numbers
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }

    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }

    # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
    #
    location ~ \.php$ {
        root           html;
        fastcgi_pass   php:9000;
        fastcgi_index  index.php;
        fastcgi_param  SCRIPT_FILENAME  /var/www/html$fastcgi_script_name;
        include        fastcgi_params;
    }
}
```

::: info
À la ligne 22, on voit que le serveur PHP est appelé `php` et qu'il écoute sur le port 9000. Le nom `php` est le nom du
service dans le fichier `compose.yml`, cf. plus bas.
:::

### Le fichier `init-db/init.sql`

```sql
CREATE TABLE IF NOT EXISTS `users`
(
    id       INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL
);

INSERT INTO `users` (username, password)
VALUES ('admin', 'admin');
INSERT INTO `users` (username, password)
VALUES ('user', 'user');
INSERT INTO `users` (username, password)
VALUES ('guest', 'guest');
```

### Le dossier `site`

#### Le fichier `index.html`

```html
<!doctype html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
        name="viewport">
  <meta content="ie=edge" http-equiv="X-UA-Compatible">
  <title>Projet</title>
</head>
<body>
<h1>Mon projet</h1>
</body>
</html>
```

#### Le fichier `index.php`

```php
<?php
phpinfo();
```

#### Le fichier `testdb.php`

```php:line-numbers
<?php
$pdo = new PDO('mysql:host=db;dbname=stage', 'root', 'root');

$stmt = $pdo->query('SELECT * FROM users');
var_dump($stmt->fetchAll());
```

## Le fichier `compose.yml`

On
créé le début du fichier YAML :

```yaml
---
version: "3.8"

services:
```

### Le serveur web

Pour le serveur web, on utilisera l'image `nginx` et on crée le service `web` :

```yaml
---
version: "3.8"

services:
  web: // [!code ++]
    image: nginx:latest // [!code ++]
    container_name: web // [!code ++]
    ports: // [!code ++]
      - "80:80" // [!code ++]
    volumes: // [!code ++]
      - ./site:/usr/share/nginx/html // [!code ++]
      - ./conf-nginx:/etc/nginx/conf.d // [!code ++]
    restart: always // [!code ++]
    networks: // [!code ++]
      - stagenet // [!code ++]

networks: // [!code ++]
  stagenet: // [!code ++]
```

::: tip
Le serveur est un serveur dédié. Il sera géré par Compose.
:::

### Le serveur PHP

Pour le serveur PHP, on utilisera l'image `php` et on crée le service `php` :

```yaml
---
version: "3.8"

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    restart: always
    networks:
      - stagenet

  php: // [!code ++]
    image: php:8.3-fpm // [!code ++]
    container_name: php // [!code ++]
    volumes: // [!code ++]
      - ./site:/var/www/html // [!code ++]
    restart: always // [!code ++]
    networks: // [!code ++]
      - stagenet // [!code ++]

networks:
  stagenet:
```

### La base de données

Pour la base de données, on utilisera l'image `mariadb` et on crée le service `db` :

```yaml
---
version: "3.8"

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    restart: always
    networks:
      - stagenet

  php:
    image: php:8.3-fpm // [!code --]
    build: // [!code ++]
      context: ./php // [!code ++]
      dockerfile: Dockerfile // [!code ++]
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    networks:
      - stagenet

  db: // [!code ++]
    image: mariadb:11 // [!code ++]
    container_name: db // [!code ++]
    volumes: // [!code ++]
      - dbdata:/var/lib/mysql // [!code ++]
      - ./init-db:/docker-entrypoint-initdb.d // [!code ++]
    environment: // [!code ++]
      MARIADB_ROOT_PASSWORD: root // [!code ++]
      MARIADB_DATABASE: stage // [!code ++]
    restart: always // [!code ++]
    networks: // [!code ++]
      - stagenet // [!code ++]

volumes: // [!code ++]
  dbdata: // [!code ++]
networks:
  stagenet:
```

::: warning
Comme on utilise MariaDB comme SGBD et que le module `pdo_mysql` est nécessaire, on doit installer le paquet `php-mysql`
dans le conteneur PHP. Pour cela, on doit créer un fichier `Dockerfile` dans le dossier `php` et on enlève
l'image `php:8.3-fpm` pour la remplacer par un build dans le fichier `compose.yml` :

```dockerfile
FROM php:8.3-fpm

RUN docker-php-ext-install pdo_mysql
``` 

:::

## Variabilisation

La variabilisation est une technique qui consiste à remplacer des valeurs par des variables. Cela permet :

- de rendre le code plus lisible, plus facile à maintenir et à réutiliser.
- de rendre le code plus sécurisé en évitant les fuites d'informations sensibles.

Exemple dans un fichier `compose.yaml` :

```yaml
---
version: "3.8"

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    restart: always
    networks:
      - stagenet

  php:
    image: php:8.3-fpm
    build:
      context: ./php
      dockerfile: Dockerfile
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    networks:
      - stagenet

  db:
    image: mariadb:11 // [!code --]
    image: ${SGBD_NAME}:${SGBD_VERSION} // [!code ++]
    container_name: db
    volumes:
      - dbdata:/var/lib/mysql
      - ./init-db:/docker-entrypoint-initdb.d
    environment:
      MARIADB_ROOT_PASSWORD: root // [!code --]
      MARIADB_ROOT_PASSWORD: ${ROOT_PASSWORD} // [!code ++]
      MARIADB_DATABASE: stage // [!code --]
      MARIADB_DATABASE: ${DB_NAME} // [!code ++]
      MARIADB_USER: ${DB_USER} // [!code ++]
      MARIADB_PASSWORD: ${DB_PASSWORD} // [!code ++]
    restart: always
    networks:
      - stagenet

volumes:
  dbdata:
networks:
  stagenet:
```

... et dans un fichier `.env` :

```txt
SGBD_NAME=mariadb
SGBD_VERSION=10.8

ROOT_PASSWORD=root

DB_USER=stagiaire
DB_PASSWORD=azerty
DB_NAME=stage
```

::: warning
Dans la version 11+ de MariaDB, il y a un souci avec la connexion. (non exploré)
:::

## Vérification du conteneur

Pour vérifier que le conteneur est toujours dans un état sain, on peut utiliser un `healthcheck` :

```yaml
---
version: "3.8"

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    healthcheck: // [!code ++]
      test: [ "CMD", "curl", "-f", "http://localhost/ready" ] // [!code ++]
      interval: 30s // [!code ++]
      timeout: 10s // [!code ++]
      retries: 3 // [!code ++]
    restart: always
    networks:
      - stagenet

  php:
    image: php:8.3-fpm
    build:
      context: ./php
      dockerfile: Dockerfile
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    networks:
      - stagenet

  db:
    image: ${SGBD_NAME}:${SGBD_VERSION}
    container_name: db
    volumes:
      - dbdata:/var/lib/mysql
      - ./init-db:/docker-entrypoint-initdb.d
    environment:
      MARIADB_ROOT_PASSWORD: ${ROOT_PASSWORD}
      MARIADB_DATABASE: stage
    restart: always
    networks:
      - stagenet

volumes:
  dbdata:
networks:
  stagenet:
```

::: tip
Pour tester que le `healthcheck` fonctionne, on peut faire un `kill -STOP <PID>` pour arrêter le conteneur et
un `kill -CONT <PID>` pour le relancer.

Pour trouver le PID, on peut utiliser `docker compose top` dans le dossier où se trouve le `compose.yaml`
ou `docker top <container_name>`.
:::

::: warning
L'URL testée doit retourner un code HTTP 200 pour que le `healthcheck` soit considéré comme réussi.
:::

## Les réglages pour les logs

Les logs vont grossir au fur et à mesure de la vie du conteneur, au bout d'un moment les fichiers peuvent devenir très
importants, pour régler les options par rapport à ces fichiers, on peut utiliser les options `logging` :

```yaml
---
version: "3.8"

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost/ready" ]
      interval: 30s
      timeout: 10s
      retries: 3
    restart: always
    logging: // [!code ++]
      driver: "json-file" // [!code ++]
      options: // [!code ++]
        max-size: "200k" // [!code ++]
        max-file: "10" // [!code ++]
    networks:
      - stagenet

  php:
    image: php:8.3-fpm
    build:
      context: ./php
      dockerfile: Dockerfile
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    networks:
      - stagenet

  db:
    image: ${SGBD_NAME}:${SGBD_VERSION}
    container_name: db
    volumes:
      - dbdata:/var/lib/mysql
      - ./init-db:/docker-entrypoint-initdb.d
    environment:
      MARIADB_ROOT_PASSWORD: ${ROOT_PASSWORD}
      MARIADB_DATABASE: stage
    restart: always
    networks:
      - stagenet

volumes:
  dbdata:
networks:
  stagenet:
```

::: info
Ici, on aura au maximum 10 fichiers de logs de 200 ko chacun. Au-delà, les fichiers les plus anciens seront supprimés.
:::

## Les ancres YAML

On veut faire le même réglage pour les logs pour tous les services, on peut utiliser les ancres YAML :

```yaml
---
version: "3.8"

x-logging: &logging // [!code ++]
  logging: // [!code ++]
    driver: "json-file" // [!code ++]
    options: // [!code ++]
      max-size: "200k" // [!code ++]
      max-file: "10" // [!code ++]

services:
  web:
    image: nginx:latest
    container_name: web
    ports:
      - "80:80"
    volumes:
      - ./site:/usr/share/nginx/html
      - ./conf-nginx:/etc/nginx/conf.d
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost/ready" ]
      interval: 30s
      timeout: 10s
      retries: 3
    restart: always
    <<: *logging // [!code ++]
    networks:
      - stagenet

  php:
    image: php:8.3-fpm
    build:
      context: ./php
      dockerfile: Dockerfile
    container_name: php
    volumes:
      - ./site:/var/www/html
    restart: always
    <<: *logging // [!code ++]
    networks:
      - stagenet

  db:
    image: ${SGBD_NAME}:${SGBD_VERSION}
    container_name: db
    volumes:
      - dbdata:/var/lib/mysql
      - ./init-db:/docker-entrypoint-initdb.d
    environment:
      MARIADB_ROOT_PASSWORD: ${ROOT_PASSWORD}
      MARIADB_DATABASE: stage
    restart: always
    <<: *logging // [!code ++]
    networks:
      - stagenet

volumes:
  dbdata:
networks:
  stagenet:
```

::: info
Ici, on a créé une ancre `&logging` qui contient les options pour les logs. On a ensuite utilisé cette ancre pour les 3
services.

Le nom de l'ancre commence par `&` et on l'utilise avec `<<: *nom_ancre` et la clé `x-` est une convention pour les
ancres.
:::