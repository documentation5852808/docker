# Commandes Docker

## `docker cp`

La commande `docker cp` permet de copier des fichiers ou des répertoires entre un conteneur et le système hôte. Par
exemple, pour copier un fichier `index.html` depuis le conteneur `web` vers le répertoire courant :

```bash
docker cp index.html web:/usr/share/nginx/html/index.html
```

... cette commande copiera le fichier `index.html` depuis le dossier courant vers le conteneur `web` dans le
répertoire `/usr/share/nginx/html/`

## `docker events`

La commande `docker events` permet de suivre les événements du serveur Docker. Par exemple, peut être utilisée pour
suivre les événements de création qui se couperait tout de suite pour suivre pourquoi il ne tient pas.

```bash
docker events
```

Exemple des événements liés à la commande :

```bash
docker run --rm busybox
```

```bash
stagiaire@deby:~$ docker events
2024-02-20T09:30:19.147223976+01:00 container create 9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e (image=busybox, name=stupefied_jemison)
2024-02-20T09:30:19.152697151+01:00 container attach 9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e (image=busybox, name=stupefied_jemison)
2024-02-20T09:30:19.180483931+01:00 network connect 7544fff62ee3780f6a913bd1eb13e92b68e101dc5d5b19c8538bce9592516dfb (container=9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e, name=bridge, type=bridge)
2024-02-20T09:30:19.405554577+01:00 container start 9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e (image=busybox, name=stupefied_jemison)
2024-02-20T09:30:19.531365180+01:00 network disconnect 7544fff62ee3780f6a913bd1eb13e92b68e101dc5d5b19c8538bce9592516dfb (container=9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e, name=bridge, type=bridge)
2024-02-20T09:30:19.547789535+01:00 container die 9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e (execDuration=0, exitCode=0, image=busybox, name=stupefied_jemison)
2024-02-20T09:30:19.552693793+01:00 container destroy 9da5d48af89596b3728baf00f986db2c733938c3a799fdd7cd08eaeeb1df5f4e (image=busybox, name=stupefied_jemison)
```

## `docker exec`

La commande `docker exec` permet d'exécuter une commande dans un conteneur en cours d'exécution. Par exemple, pour
exécuter une commande bash dans un conteneur nommé `web` :

```bash
docker exec -it web bash
```

- `-i` ou `--interactive` : ouvre un terminal interactif.
- `-t` ou `--tty` : alloue un pseudo-tty => attache un terminal.

... cette commande ouvrira un terminal interactif dans le conteneur `web`.

## `docker top`

La commande `docker top` permet de voir les processus en cours d'exécution dans un conteneur. Par exemple, pour voir les
processus en cours d'exécution dans un conteneur nommé `jojotique_api` :

```bash
❯ docker top jojotique_api
UID                 PID                 PPID                C                   STIME               TTY                 TIME                CMD
root                251961              251942              0                   Feb19               ?                   00:00:00            /bin/bash /home/entrypoint.sh
root                252002              251961              0                   Feb19               ?                   00:00:02            node /usr/local/bin/pnpm start
root                252054              252002              0                   Feb19               ?                   00:00:00            sh -c NODE_ENV=production node dist/server.js
root                252055              252054              0                   Feb19               ?                   00:00:05            node dist/server.js
```

## `docker rm`

La commande `docker rm` permet de supprimer un ou plusieurs conteneurs. Par exemple, pour supprimer un conteneur nommé :

```bash
docker rm web
```

... cette commande supprimera le conteneur nommé `web`.

On peut aussi supprimer tous les conteneurs :

```bash
docker rm $(docker ps -aq)
```

## `docker run`

Pour voir le détail, voir [Premiers pas](/start/first-steps.html#creer-et-lancer-un-conteneur).

### Options supplémentaires

On peut le lancer avec des options :

```bash
docker run -d <image>
```

- `--detach` ou `-d` : pour le lancer en mode détaché

```bash
docker run --name <name> <image>
```

- `--name` : pour lui donner un nom
  Exemple :

```bash
docker run --name webserver nginx:1.24
```

```bash
docker run --rm <image>
```

- `--rm` : pour le supprimer après son arrêt

::: warning
Attention, si on précise `--rm`, le conteneur sera supprimé après son arrêt et toutes les informations non persistées
seront perdus : logs, modifications, ...
:::

Certaines images s'arrêtent immédiatement après leur lancement. Pour les garder en vie, on doit leur attacher un
terminal :

```bash
docker run -it <image>
```

- `--interactive` ou `-i` : pour le lancer en mode interactif
- `--tty` ou `-t` : pour lui donner un terminal

Pour les garder en vie un conteneur, on peut lui faire lancer une instruction qui reste en vie :

```bash
docker run -d busybox sleep infinity
```

Pour s'y connecter dans un second temps, on peut utiliser la commande `docker exec` :

```bash
docker exec -it <container> bash
```