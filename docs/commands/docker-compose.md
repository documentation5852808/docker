# Commandes Docker Compose

## `docker compose config`

La commande `docker compose config` permet de valider un fichier de configuration de Docker Compose.

```bash
stagiaire@deby:~/stagiaire$ docker compose config
name: stagiaire
services:
  busy:
    command:
      - sleep
      - infinity
    container_name: busy
    environment:
      PASSWORD: secret
      USER: bob
    image: busybox:latest
    networks:
      stagenet: null
    ports:
      - mode: ingress
        target: 9000
        published: "9000"
        protocol: tcp
    restart: always
    volumes:
      - type: volume
        source: data
        target: /partage
        volume: {}
      - type: bind
        source: /home/stagiaire/stagiaire/config
        target: /opt/config
        bind:
          create_host_path: true
networks:
  stagenet:
    name: stagiaire_stagenet
volumes:
  data:
    name: stagiaire_data
```

## `docker compose down`

La commande `docker compose down` permet d'arrêter et de supprimer les conteneurs, les réseaux et les volumes créés par
un projet Docker Compose.

```bash
stagiaire@deby:~/stagiaire$ docker compose down -v
[+] Running 3/3
 ✔ Container busy              Removed                                                                                                                                                                                                                                               0.2s 
 ✔ Volume stagiaire_data       Removed                                                                                                                                                                                                                                               0.0s 
 ✔ Network stagiaire_stagenet  Removed
```

::: warning
L'option `-v` ou `--volumes` permet de supprimer les volumes associés au projet, s'ils ne sont pas externes. Sans cette
option, seuls les conteneurs et le réseau, également s'il n'est pas externe, seront supprimés.
:::

## `docker compose ls`

La commande `docker compose ls` permet de lister les services d'un projet Docker Compose. Il donnera le nom du service,
l'état du service et le fichier de configuration utilisé.

```bash
stagiaire@deby:~/stagiaire$ docker compose ls
NAME                STATUS              CONFIG FILES
stagiaire           running(1)          /home/stagiaire/stagiaire/compose.yml
```

## `docker compose <file> ps`

La commande `docker compose <file> ps` permet de lister les services d'un projet Docker Compose. Il donnera plus
d'informations qu'un simple `docker compose ls`.

```bash
stagiaire@deby:~/stagiaire$ docker compose -f /home/stagiaire/stagiaire/compose.yml ps
NAME      IMAGE            COMMAND            SERVICE   CREATED         STATUS         PORTS
busy      busybox:latest   "sleep infinity"   busy      3 minutes ago   Up 3 minutes   0.0.0.0:9000->9000/tcp, :::9000->9000/tcp
```

::: warning
Il est possible de spécifier le fichier de configuration avec l'option `-f` ou `--file` s'il n'a pas le nom par défaut
ou s'il n'est pas dans le répertoire courant.
:::

## `docker compose top`

La commande `docker compose top` permet de voir les processus en cours d'exécution dans un service d'un projet Docker
Compose.

```bash
stagiaire@deby:~/stagiaire$ docker compose top
busy
UID    PID    PPID   C    STIME   TTY   TIME       CMD
root   1245   1224   0    10:18   ?     00:00:00   sleep infinity
```