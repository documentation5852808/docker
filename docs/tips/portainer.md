# Portainer

Portainer est une interface web pour gérer des conteneurs Docker. Elle permet de visualiser les conteneurs, les images,
les volumes, les réseaux, etc. Elle permet aussi de créer et de modifier des conteneurs, des images, des volumes, des
réseaux, etc.

## Installation

Pour installer Portainer, on peut utiliser l'image `portainer/portainer` de Docker. On peut la télécharger avec la
commande suivante :

```yaml
---
services:
  portainer:
    image: portainer/portainer-ce
    container_name: portainer
    ports:
      - 9000:9000
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - portainer_data:/data
    restart: always

volumes:
  portainer_data:
```

## Utilisation

Pour utiliser Portainer, on peut se rendre à l'adresse `http://localhost:9000` dans un navigateur web. On peut se créer
un compte et se connecter. On peut ensuite gérer des conteneurs, des images, des volumes, des réseaux, etc.
