# Analyse de vulnerabilités

Trivy est un outil d'analyse de vulnérabilités pour les conteneurs Docker. Il permet de scanner les images Docker et de
trouver les vulnérabilités connues.

## Utilisation

```bash
docker run -it -v /run/docker.sock:/var/run/docker.sock aquasec/trivy image <image>
```