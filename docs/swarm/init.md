# Initialiser un cluster Swarm

## Initialisation

Pour initialiser un cluster Swarm, on utilise la commande `init` :

```bash
docker swarm init --advertise-addr <adresse_ip>
```

Il va alors générer un token pour joindre le cluster. Ce token est de la forme :

```bash
docker swarm join --token <token> <adresse_ip>
```

Pour retrouver le token, on peut utiliser la commande `join-token` :

```bash
docker swarm join-token worker
```
