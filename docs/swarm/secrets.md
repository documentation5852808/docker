# Gestion des secrets

## Création d'un secret

On a deux solutions pour créer un secret :

```bash
docker secret create <nom_secret> <fichier>

# exemple

docker secret create db_password db_password.txt
```

Ou alors, on peut utiliser l'entrée standard :

```bash
docker secret create <nom_secret> -

# exemple

docker secret create db_password -
```

... et vous écrivez le mot de passe dans l'entrée standard.

Si jamais on ne peut pas utiliser l'entrée standard car on du monde autour de nous, on peut ajouter une étape avec la
commande `read` et `echo` :

```bash
read -s <var_name>

# exemple

read -s db_password
```

... puis on utilise `echo` pour écrire le mot de passe dans un fichier :

```bash
echo $db_password | docker secret create db_password -
```