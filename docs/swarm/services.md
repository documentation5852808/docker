# Services

## Introduction

Les services sont des nouveaux objets dans Docker qui n'existent que sur Swarm. Ils ne se déploient qu'à l'aide de
commande CLI.

Derrière un service, il peut y avoir plusieurs conteneurs déployés.

## Gestion des services

### Création d'un service

Pour créer un service, on utilise la commande `service create` :

```bash
docker service create --name www nginx
```

### Lister les conteneurs d'un service

Pour lister les conteneurs d'un service, on utilise la commande `ps` :

```bash
docker service ps <nom_service>
```

On aura alors la liste des conteneurs du service avec leur ID, le nœud sur lequel ils tournent, leur état, etc.

```text
ID             NAME      IMAGE          NODE      DESIRED STATE   CURRENT STATE                ERROR     PORTS
uyt9pnrwhxnb   www.1     nginx:latest   swarm-1   Running         Running about a minute ago             
```

::: info
On voit le `NODE` sur lequel tourne le conteneur. C'est Swarm qui gère la répartition de la charge et qui décide sur
quel nœud le conteneur doit tourner.
:::

### Multiplier les replicas

Pour multiplier les replicas d'un service, on utilise la commande `scale` :

```bash
docker service scale www=3
```

Swarm va alors créer deux nouveaux conteneurs pour arriver à trois conteneurs pour le service `www`.

### Supprimer un service

Pour supprimer un service, on utilise la commande `rm` :

```bash
docker service rm www
```

## Resilience

Quand un replica tombe, car le nœud sur lequel il tourne tombe, Swarm va le relancer sur un autre nœud.

Exemple, on coupe une VM de notre cluster. On fait la commande :

```bash
watch -n 1 docker service ps www
```

... pour voir en temps réel les changements. On commence avec :

![Watch replicas](/watch-replicas.png)

On coupe une VM, on voit que le replica `swarm-2` est en `Shutdown` :

![Shutdown replica](/shutdown-replica.png)

## Voir les logs d'un service

Pour voir les logs d'un service, on utilise la commande `logs` :

```bash
docker service logs www
```