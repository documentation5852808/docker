# Swarm

Swarm est un orchestrateur de conteneurs.

## Mise en place de l'environnement

Pour simuler plusieurs serveurs, mise en place de trois machines virtuelles avec VirtualBox, on clone la première image
trois fois.

Couper la connexion réseau de ces machines :

```bash
ifdown enp0s8
```

Changer l'IP sur laquelle elle écoute :

```bash
vim /etc/network/interfaces
```

... et mettre :

- swarm-1 : `192.168.56.200`
- swarm-2 : `192.168.56.210`
- swarm-3 : `192.168.56.220`

Redémarrer la connexion réseau :

```bash
ifup enp0s8
```

Modification du hostname :

```bash
hostnamectl set-hostname swarm-<nb>
```

## Pré-requis Swarm

- Il faut une communication entre les machines.
- Les ports suivants doivent être ouverts :
    - `2377/TCP` : pour la communication entre les managers
    - `7946/TCP` : pour la communication entre les noeuds
    - `4789/UDP` : pour la communication entre les noeuds pour le réseau overlay
- Docker doit être installé sur toutes les machines.
- Docker doit être actif sur toutes les machines.

## Qu'est-ce qu'un orchestrateur ?

Solution de gestion des conteneurs multi-nodes assurant :

- Répartition de la charge sur les nœuds (on peut mettre des contraintes de placement)
- Tolerance de panne : si un nœud tombe, les conteneurs sont relancés sur un autre nœud
- Mise à jour avec possibilités limitées de rollback

## Solutions d'orchestration

- Docker Swarm (Docker)
- Kubernetes (CNCF/Google)
    - Nodes : 5000
    - Pods : 150000
    - Conteneurs : 300000
    - Pods/nœud : 110
- OpenShift (Red Hat)
- Mesos (Apache => pas que conteneurs)
- Nomad (HashiCorp)

## Structure d'un cluster

- Un cluster est composé de plusieurs nœuds, un nœud peut être :
    - Machine physique
    - Machine virtuelle
    - Conteneur (à des fins de test uniquement)

## Types de nœuds

### Nœud de gestion

Leurs noms :

- Swarm : manager
- Kubernetes : control-plane
- OpenShift : master

Ils ont pour rôle :

- configurer le cluster
- ordonnancer les ressources
- prendre en charge les demandes et déploiement des ressources (via des API)

En Swarm, un nœud de travail peut être promu en nœud de contrôle et inversement.

### Nœud de travail

Ce sont les nœuds qui exécutent les conteneurs. On les appelle aussi "worker nodes".