# Gestion des stacks

## Introduction

Les stacks sont des groupes de services qui sont définis dans un fichier de configuration. Les stacks sont utilisés pour
déployer des applications dans un environnement Docker Swarm. Les stacks sont un moyen de définir et de gérer des
applications multi-conteneurs. Les stacks sont définis dans un fichier de configuration appelé fichier de composition.
Les fichiers de composition sont écrits en YAML et décrivent les services, les réseaux et les volumes qui composent une
application.

Il pourrait être comparé à `compose` mais pour `swarm`.

## Création d'une stack

```yaml
---
services:
  web:
    image: "bob2606/hello-http:latest"
    ports:
      - "80:8080"
    environment:
      - "MSG=Test Swarm"
    networks:
      hellonet:

networks:
  hellonet:
```

On peut tester le fichier avec :

```bash
docker stack config -c hello-http.yml
```

On reçoit :

```yaml
version: "3.12"
services:
  web:
    environment:
      MSG: Test Swarm
    image: bob2606/hello-http:latest
    networks:
      hellonet: null
    ports:
      - mode: ingress
        target: 8080
        published: 80
        protocol: tcp
networks:
  hellonet: { }
```

Et on le crée avec la commande suivante :

```bash
docker stack deploy hello -c hello-http.yml
```

## Lister les services et des conteneurs dans la stack

Liste des services :

```bash
docker stack services hello
```

On reçoit :

```text
ID             NAME        MODE         REPLICAS   IMAGE                       PORTS
94vrou3rxq0m   hello_web   replicated   1/1        bob2606/hello-http:latest   *:80->8080/tcp
```

Liste des conteneurs :

```bash
docker stack ps hello
```

On reçoit :

```text
ID             NAME          IMAGE                       NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
6amq8q5i2dhf   hello_web.1   bob2606/hello-http:latest   swarm-1   Running         Running 3 minutes ago             
```

## Mise à l'échelle d'un service

On ne peut pas mettre à l'échelle un service directement avec `docker service scale` car c'est une stack. Il faut donc
le faire au niveau du service :

```bash
docker service scale hello_web=3
```

## Rollback

La stack stocke les `specs` des services. Si on modifie, par exemple, le nombre de replicas, la stack va stocker
la `spec` actuelle et l'ancienne `spec`. On peut alors revenir à l'ancienne `spec` avec la commande suivante :

```bash
docker service rollback hello_web
```

::: warning
On ne peut `rollback` qu'une seule fois, car la stack ne stocke que deux `Specs` et `PreviousSpec`. Quand on rollback,
il va faire la bascule et la `Spec` actuelle devient `PreviousSpec` et la `PreviousSpec` devient `Spec`.
:::

## Cas d'usage

Création d'une stack `lamp` avec les services suivants :

```yaml
---
services:
  web:
    image: "nginx:latest"
    ports:
      - "80:80"
    volumes:
      - "./site:/usr/share/nginx/html"
      - "/etc/nginx/conf.d:/etc/nginx/conf.d"
    networks:
      hellonet:

  php:
    image: "php:8.3-fpm"
    volumes:
      - "./site:/var/www/html"
    networks:
      hellonet:

networks:
  hellonet:
```

::: warning
Si on part sur cette configuration on aura un problème, car les volumes n'ont pas de sens en Swarm. En effet, dans une
stack, on a plusieurs conteneurs, ils doivent partager la même base, c'est pour ça qu'il est mieux de se baser sur un
montage bind.
:::

### Mise en place d'un montage bind

Sur le nœud manager, ici `swarm-1`, on install `nfs-kernel-server` :

```bash
apt install -y nfs-kernel-server
```

Dans le fichier `/etc/exports`, on ajoute :

```text
/home/stagiaire/swarm/hello/site *(rw,no_subtree_check,no_root_squash,fsid=0)
```

On redémarre le service en `su -`:

```bash
exportfs -a
```

Sur les autres nœuds, on installe `nfs-common` :

```bash
apt install -y nfs-common
```

On ajoute le volume dans la stack :

```yaml
---
services:
  web:
    image: "nginx:latest"
    ports:
      - "80:80"
    volumes:
      - "site:/usr/share/nginx/html"
      - "conf:/etc/nginx/conf.d"
    networks:
      hellonet:

  php:
    image: "php:8.3-fpm"
    volumes:
      - "site:/var/www/html"
    networks:
      hellonet:

networks:
  hellonet:

volumes: // [!code ++]
  site: // [!code ++]
    driver: local // [!code ++]
    driver_opts: // [!code ++]
      type: nfs // [!code ++]
      o: addr=192.168.56.200,vers=4.2 // [!code ++]
      device: ":/site" // [!code ++]

  conf: // [!code ++]
    driver: local // [!code ++]
    driver_opts: // [!code ++]
      type: nfs // [!code ++]
      o: addr=192.168.56.200,vers=4.2 // [!code ++]
      device: ":/conf" // [!code ++]
```

On peut alors déployer la stack :

```bash
docker stack deploy lamp -c lamp.yml
```

On ajoute maintenant la base de données :

```yaml
---
services:
  web:
    image: "nginx:latest"
    ports:
      - "80:80"
    volumes:
      - "site:/usr/share/nginx/html"
      - "conf:/etc/nginx/conf.d"
    networks:
      hellonet:

  php:
    image: "php:8.3-fpm"
    volumes:
      - "site:/var/www/html"
    networks:
      hellonet:

  db: // [!code ++]
    image: "mariadb:latest" // [!code ++]
    volumes: // [!code ++]
      - "db:/var/lib/mysql" // [!code ++]
    networks: // [!code ++]
      hellonet: // [!code ++]

networks:
  hellonet:

volumes:
  site:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/site"

  conf:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/conf"

  db: // [!code ++]
    driver: local // [!code ++]
    driver_opts: // [!code ++]
      type: nfs // [!code ++]
      o: addr=192.168.56.200,vers=4.2 // [!code ++]
      device: ":/db" // [!code ++]
```

Pour le mot de passe de la base de données, on va devoir utiliser un nouveau type d'objet :
un [`secret`](/swarm/secrets.html). On va donc créer un fichier `db_password.txt` avec le mot de passe dedans :

```text
azerty
```

On va ensuite créer le secret :

```bash
docker secret create db_password db_password.txt
```

On peut ajouter lier le secret à la stack :

```yaml
---
services:
  web:
    image: "nginx:latest"
    ports:
      - "80:80"
    volumes:
      - "site:/usr/share/nginx/html"
      - "conf:/etc/nginx/conf.d"
    networks:
      hellonet:

  php:
    image: "php:8.3-fpm"
    volumes:
      - "site:/var/www/html"
    networks:
      hellonet:

  db:
    image: "mariadb:latest"
    volumes:
      - "db:/var/lib/mysql"
    secrets: // [!code ++]
      - db_password // [!code ++]
    networks:
      hellonet:

networks:
  hellonet:

volumes:
  site:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/site"

  conf:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/conf"

  db:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/db"

secrets: // [!code ++]
  db_password: // [!code ++]
    external: true // [!code ++]
```

Une fois le secret ajouté, ça crée un fichier dans le dossier `/run/secrets` dans le conteneur, on donc le lier à la
bdd :

```yaml
---
services:
  web:
    image: "nginx:latest"
    ports:
      - "80:80"
    volumes:
      - "site:/usr/share/nginx/html"
      - "conf:/etc/nginx/conf.d"
    networks:
      hellonet:

  php:
    image: "php:8.3-fpm"
    volumes:
      - "site:/var/www/html"
    networks:
      hellonet:

  db:
    image: "mariadb:latest"
    volumes:
      - "db:/var/lib/mysql"
    secrets:
      - db_password
    environment: // [!code ++]
      MYSQL_ROOT_PASSWORD_FILE: /run/secrets/db_password // [!code ++]
    networks:
      hellonet:

networks:
  hellonet:

volumes:
  site:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/site"

  conf:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/conf"

  db:
    driver: local
    driver_opts:
      type: nfs
      o: addr=192.168.56.200,vers=4.2
      device: ":/db"

secrets:
  db_password:
    external: true
```
