# Gestion des nœuds

## Liste des nœuds

Pour lister les nœuds du cluster, on utilise la commande `node` :

```bash
docker node ls
```

On aura alors un tableau avec les nœuds, leur état, leur rôle, leur version, leur OS, leur architecture et leur ID.

```text
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
18y01ig9ppb3zpkl1s2sip28v *   swarm-1    Ready     Active         Leader           25.0.3
hpillfjzkejvgm11t6ql6itrx     swarm-2    Ready     Active                          25.0.3
nlv16b7d0ea3sqxc4ob765x4j     swarm-3    Ready     Active                          25.0.3
```

::: info
Le nœud avec une étoile est le nœud qui répond à la commande `docker node ls`.
:::

## Promotion d'un nœud

Pour promouvoir un nœud en manager, on utilise la commande `promote` :

```bash
docker node promote <nom_noeud>

# exemple

docker node promote swarm-2
```

## Dégradation d'un nœud

Pour dégrader un nœud en worker, on utilise la commande `demote` :

```bash
docker node demote <nom_noeud>
```

## Suppression d'un nœud

Pour supprimer un nœud depuis le manager, on utilise la commande `rm` :

```bash
docker node rm <nom_noeud>
```

Si on veut quitter, on utilise la commande `leave` :

```bash
docker swarm leave
```

::: warning
Pour pouvoir supprimer un nœud, il doit être en état `Down` et donc avoir fait un `leave` du cluster ou on fait un `-f`
pour le supprimer.

Si on fait un `-f`, le nœud est toujours partie prenante du cluster, mais il n'est plus dans la liste au niveau du
manager.
:::

## Mise en pause d'un nœud

Pour mettre en pause un nœud, on utilise la commande `update` :

```bash
docker node update --availability pause <nom_noeud>
```

::: info
Dans un nœud en pause, les conteneurs ne sont pas déplacés, mais aucun nouveau conteneur ne peut être lancé.
:::

## Remise en service d'un nœud

Pour remettre en service un nœud, on utilise la commande `update` :

```bash
docker node update --availability active <nom_noeud>
```

## Désactivation d'un nœud

Pour désactiver un nœud, on utilise la commande `update` :

```bash
docker node update --availability drain <nom_noeud>
```

::: info
Dans un nœud en drain, les conteneurs sont déplacés sur d'autres nœuds.
:::