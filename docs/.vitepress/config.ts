import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: 'Docker',
  description: 'Documentation docker',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Documentation', link: '/start/installation' },
    ],

    sidebar: [
      {
        text: 'Démarrer',
        items: [
          { text: 'Installation', link: '/start/installation' },
          { text: 'Concept', link: '/start/concept' },
          { text: 'Premiers pas', link: '/start/first-steps' },
        ],
      },
      {
        text: 'Docker',
        items: [
          { text: 'Image', link: '/docker/image' },
          { text: 'Container', link: '/docker/container' },
          { text: 'Network', link: '/docker/network' },
          { text: 'Volume', link: '/docker/volume' },
          { text: 'Variable d\'environnement', link: '/docker/env-var' },
          { text: 'Dockerfile', link: '/docker/dockerfile' },
          { text: 'Registry', link: '/docker/registry' },
          { text: 'Reverse proxy', link: '/docker/reverse-proxy' },
          { text: 'Sécurité', link: '/docker/security' },
          { text: 'Logs', link: '/docker/logs' },
        ],
      },
      {
        text: 'Docker compose',
        items: [
          { text: 'Introduction', link: '/compose/intro' },
          { text: 'Syntaxe', link: '/compose/syntax' },
          { text: 'Stack complète', link: '/compose/stack' },
          { text: 'Registry', link: '/compose/registry' },
        ],
      },
      {
        text: 'Swarm',
        items: [
          { text: 'Introduction', link: '/swarm/intro' },
          { text: 'Initialisation', link: '/swarm/init' },
          { text: 'Gestion des nœuds', link: '/swarm/node-settings' },
          { text: 'Gestion des services', link: '/swarm/services' },
          { text: 'Gestion des stacks', link: '/swarm/stacks' },
          { text: 'Gestion des secrets', link: '/swarm/secrets' },
        ],
      },
      {
        text: 'Astuces',
        items: [
          { text: 'Interface graphique', link: '/tips/portainer' },
          { text: 'Analyse de vulnérabilité', link: '/tips/trivy' },
        ],
      },
      {
        text: 'Commandes',
        items: [
          { text: 'docker', link: '/commands/docker' },
          { text: 'docker-compose', link: '/commands/docker-compose' },
        ],
      },
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' },
    ],

    outline: [2, 4],
  },

  outDir: '../public',
  base: '/docker/',
})
